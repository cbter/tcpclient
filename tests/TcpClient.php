<?php

namespace Lambq\Tcpclient\tests;

use Lambq\Tcpclient\Panel\Tcp;

class TcpClient
{
    protected $client;

    protected $isConnected;
    public function __construct()
    {
        // 创建一个tcp 连接
        $this->client = new Tcp();

        // 保存连接状态值
        $this->isConnected = $this->client->set()->proxy('207.148.122.41', '7091')->connect();

        if ($this->isConnected)
        {

        } else {
            $this->client->connect();
        }
    }

}