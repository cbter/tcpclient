<?php

namespace Lambq\Tcpclient\tests;

use Lambq\Tcpclient\Panel\Tcp;

class ManyTcpClient
{
    protected $tcp;

    protected $name;

    protected $client;

    public function __construct($name)
    {
        $this->name = $name;
        // 创建一个tcp 连接
        if (array_key_exists($this->name, $this->tcp))
        {
            $this->tcp[$this->name];
        } else {
            $this->tcp[$this->name] = new Tcp();
        }
    }

    // 获取对应的 tcp 连接
    public function client()
    {
        $this->client   = $this->tcp[$this->name];
        return $this;
    }
}