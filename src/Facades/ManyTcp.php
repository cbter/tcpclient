<?php

namespace Lambq\Tcpclient\Facades;

use Illuminate\Support\Facades\Facade;

class ManyTcp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ManyTcp';
    }
}
