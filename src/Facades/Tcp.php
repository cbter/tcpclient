<?php

namespace Lambq\Tcpclient\Facades;

use Illuminate\Support\Facades\Facade;

class Tcp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Tcp';
    }
}
