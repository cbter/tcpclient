<?php

namespace Lambq\Tcpclient;

use Illuminate\Support\ServiceProvider;
use Lambq\Tcpclient\Panel\Tcp;
use Lambq\Tcpclient\Panel\ManyTcp;

class TcpClientServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/tcpClient.php','tcpClient');
        $this->registerPublishing();
    }

    public function register()
    {
        $this->app->singleton('Tcp',function (){
            return new Tcp();
        });

        $this->app->singleton('ManyTcp',function (){
            return new ManyTcp();
        });

    }

    /**
     * @return array
     */
    public function provides()
    {
        return ['Tcp', 'ManyTcp'];
    }

    /**
     * 资源发布注册.
     *
     * @return void
     */
    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/config/tcpClient.php' => config_path('tcpClient.php'), 'lambq-tcpClient-config']);
        }
    }
}
