<?php

use Lambq\Tcpclient\Panel\Tcp;


if (!function_exists('connectToSend'))
{
    /**
     * 发送 tcp 数据
     * 格式 array 数组
     *
     * 成功的数组
     * [
     *     'status' => 'ok',
     *     'data'   => '',
     *     'msg'    => [],
     * ]
     *
     * 失败的数组
     * [
     *     'status' => 'fail',
     *     'data'   => '',
     *     'msg'    => [],
     * ]
     */
    function connectToSend($message)
    {
        $tcp = new Tcp();
        return $tcp->connectToSend($message);
    }
}


if (!function_exists('connectToReceive'))
{
    /**
     * 获取 tcp 返回数据
     * 格式 array 数组
     *
     * 成功的数组
     * [
     *     'status' => 'ok',
     *     'data'   => [],
     *     'msg'    => [],
     * ]
     *
     * 失败的数组
     * [
     *     'status' => 'fail',
     *     'data'   => [],
     *     'msg'    => [],
     * ]
     */
    function connectToReceive($proc)
    {
        $tcp = new Tcp();

        return $tcp->connectToReceive($proc);
    }
}
