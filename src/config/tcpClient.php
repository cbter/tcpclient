<?php

return [
    'host'      => env('TCP_HOST', '127.0.0.1'), // 可以是域名，也可以是IP
    'port'      => env('TCP_PORT', ''), //
    'timeout'   => env('TCP_TIMEOUT', 6), // 总超时 1 为 1秒 0.2 为 200ms
    'connect'   => env('TCP_CONNECT', 5), // 连接超时
    'write'     => env('TCP_WRITE', 10), // 发送数据超时
    'read'      => env('TCP_READ', 2), // 读取数据超时
    'ssl'       => env('TCP_SSL', false), // 是否开启安全连接

    /*
    |--------------------------------------------------------------------------
    | Lambq-TcpClient install directory
    |--------------------------------------------------------------------------
    |
    | The installation directory of the controller and routing configuration
    | files of the administration page. The default is `app/Admin`, which must
    | be set before running `artisan admin::install` to take effect.
    |
    */
    'directory' => app_path('TcpClient'),
];